import { Pipe, PipeTransform } from '@angular/core';
import { Spell } from '../interfaces/spell';

@Pipe({
  name: 'spellFilter'
})

export class SpellFilterPipe implements PipeTransform {

  transform(items: Spell[], filter: string): any {
    if (!items || !filter || filter.length == 0) {
      return items;
    }
    return items.filter(item => item.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
  }

}
