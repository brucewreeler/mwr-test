import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class SpellServiceService {
  constructor(private http: HttpClient) { }
  
  baseUrl = "http://www.dnd5eapi.co/api/spells/"

  public spells

  getSpells(){
    return this.http.get(this.baseUrl)
  }

  getSpellUrl(url){

    const headerDict = {
      'Content-Type': 'application/json; charset=utf-8',
    }
    const requestOptions = {                                                                                                                                                                                 
      headers: new HttpHeaders(headerDict), 
    };
    return this.http.get(url, requestOptions)
  }

}
