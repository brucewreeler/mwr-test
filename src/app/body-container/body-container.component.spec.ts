import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyContainerComponent } from './body-container.component';
import { TitleBarComponent } from '../title-bar/title-bar.component'
import { BodyContentComponent } from '../body-content/body-content.component';
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { SpellsComponent } from '../spells/spells.component';
import { ClassesComponent } from '../classes/classes.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

describe('BodyContainerComponent', () => {
  let component: BodyContainerComponent;
  let fixture: ComponentFixture<BodyContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatListModule,
        MatIconModule,
        MatButtonModule
      ],
      declarations: [ 
        BodyContainerComponent,
        TitleBarComponent,
        BodyContentComponent,
        NavBarComponent,
        SpellsComponent,
        ClassesComponent
       ]
    })
    .compileComponents();
  }));

});
