import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { BodyContainerComponent } from './body-container/body-container.component'
import { TitleBarComponent } from './title-bar/title-bar.component'
import { BodyContentComponent } from './body-content/body-content.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SpellsComponent } from './spells/spells.component';
import { ClassesComponent } from './classes/classes.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatListModule,
        MatIconModule,
        MatButtonModule
      ],
      declarations: [
        AppComponent,
        BodyContainerComponent,
        TitleBarComponent,
        BodyContentComponent,
        NavBarComponent,
        SpellsComponent,
        ClassesComponent
      ],
    }).compileComponents();
  }));

});
