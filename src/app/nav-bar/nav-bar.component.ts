import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  @Output() selectContent = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  changeContent(content: string){
    console.log(content)
    this.selectContent.emit(content)
  }
}
