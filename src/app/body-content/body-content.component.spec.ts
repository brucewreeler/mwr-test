import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyContentComponent } from './body-content.component';
import { TitleBarComponent } from '../title-bar/title-bar.component'
import { NavBarComponent } from '../nav-bar/nav-bar.component';
import { SpellsComponent } from '../spells/spells.component';
import { ClassesComponent } from '../classes/classes.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

describe('BodyContentComponent', () => {
  let component: BodyContentComponent;
  let fixture: ComponentFixture<BodyContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatListModule,
        MatIconModule,
        MatButtonModule
      ],
      declarations: [ 
        BodyContentComponent,
        TitleBarComponent,
        NavBarComponent,
        SpellsComponent,
        ClassesComponent
       ]
    })
    .compileComponents();
  }));

});
