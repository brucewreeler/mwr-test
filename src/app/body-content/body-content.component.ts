import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-body-content',
  templateUrl: './body-content.component.html',
  styleUrls: ['./body-content.component.css']
})
export class BodyContentComponent implements OnInit {

  displayedContent = ""

  @Input() set contentToDisplay(contentToDisplay){
    this.displayedContent = contentToDisplay
    console.log(contentToDisplay)
  }

  constructor() { }

  ngOnInit() {
  }

}
