import { SpellFilterPipe } from './pipes/spell-filter.pipe';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import { BodyContainerComponent } from './body-container/body-container.component';
import { BodyContentComponent } from './body-content/body-content.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { SpellsComponent } from './spells/spells.component';
import { HttpClientModule } from '@angular/common/http';
import { ScrollingModule } from '@angular/cdk/scrolling';
import {MatButtonModule} from '@angular/material/button';
import { ClassesComponent } from './classes/classes.component';
import { TitleBarComponent } from './title-bar/title-bar.component'; 
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    BodyContainerComponent,
    BodyContentComponent,
    NavBarComponent,
    SpellsComponent,
    ClassesComponent,
    TitleBarComponent,
    SpellFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatListModule,
    MatIconModule,
    HttpClientModule,
    ScrollingModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatInputModule,
    FormsModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
