import { Component, OnInit } from '@angular/core';
import { SpellServiceService } from '../services/spellService/spell-service.service';


@Component({
  selector: 'app-spells',
  templateUrl: './spells.component.html',
  styleUrls: ['./spells.component.less']
})
export class SpellsComponent implements OnInit {
  
  constructor(private spellService: SpellServiceService) { }
  
  public spellList:any = null
  public spell:any = null
  spellName = '';
  focused = false;


  ngOnInit() {
   this.populateSpells()
  }

  populateSpells(){
    this.spellService.getSpells().subscribe( result => {
      this.spellList = result
      console.log(this.spellList)
      return result
    });
  }

  getSpell(spell){
    this.spellService.getSpellUrl(spell).subscribe( result => {
      this.spell = result
      //console.log(this.spell.desc)
      this.spell.components.forEach(it =>{
        if(it == 'V'){
          this.spell.verbal = true
        }else if(it =='M'){
          this.spell.materials = true
        }else if(it =='S'){
          this.spell.somatic = true
        }
      })
      if (this.spell.level == '0'){
        this.spell.level = "Cantrip"
      }
      let tempArr = []
      let i = 0
      this.spell.desc.forEach(element => {
        tempArr[i] = this.htmlDecode(element)
        i++;
      });
      this.spell.desc = tempArr
      //console.log(this.spell.desc)
    });
    
  }

  htmlDecode(input){
    //Because the api isn't decoding html special characters
    input = input.replace(/â€™/g,"'")
    input = input.replace(/â€�/g,"")
    input = input.replace(/â€œ/g,"")
    input = input.replace(/â€”/g,"\"")
    return input
  }

}
