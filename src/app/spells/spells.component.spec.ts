import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpellsComponent } from './spells.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import {MatInputModule} from '@angular/material/input';
import { FormsModule } from '@angular/forms';
import { SpellFilterPipe } from '../pipes/spell-filter.pipe';

describe('SpellsComponent', () => {
  let component: SpellsComponent;
  let fixture: ComponentFixture<SpellsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatListModule,
        MatIconModule,
        MatButtonModule,
        HttpClientModule,
        MatInputModule,
        FormsModule
      ],
      declarations: [ SpellsComponent,
        SpellFilterPipe ]
    })
    .compileComponents().then(()=>{
      fixture = TestBed.createComponent(SpellsComponent)
      component = fixture.componentInstance
    });
  }));

  //Unit tests to check Spells

  it("Should have at least one spell ###########################",async(() =>{
      const spells = component.populateSpells()
      expect(spells).not.toBeNull();
  }));

  it("Spell at pos 0 should return full spell ###########################",async(() =>{
    const spell = component.getSpell("http://www.dnd5eapi.co/api/spells/7")
    expect(spell).not.toBeNull();
  }));    
      
  

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(SpellsComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
  // });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
